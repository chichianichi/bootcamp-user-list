# User List
<p>Proyecto de práctica para el desarrollo Frontend de páginas dinámicas con Angular.<br>
La característica principal es la comunicación HTTP con una API, así traer información y presentarla en pantalla.
 <br>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con Angular.</li>
    <li>Uso de [HttpClient](https://angular.io/api/common/http/HttpClient)</li>
    <li>Peticiones a REST API ([Reqres](https://reqres.in/))</li>
    <li>Vistas con HTML.</li>
    <li>Estilos con CSS.</li>
    <li>Uso de Token para mostrar información.</li>
    <li>Elementos dinámicos.</li>
    <li>Enrutamiento.</li>           
</ul>

<h2>Las vistas con las que cuenta la web app son las siguientes:</h2>

<ul>
    <li>/login  :  email": "eve.holt@reqres.in", "password": "cityslicka".</li>
    <li>/user-list   :   Muestra la lista con los usuarios recatados y se les puede hacer click en cualquier entrada para ver más información del usuario.</li>
    <li>/user-detail   :  recibe como parametro el id de un usuario para mostrar su información completa.</li>       
</ul>


