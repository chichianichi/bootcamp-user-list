import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {

  constructor(
    private _http: HttpClient
  ) { }

  public getUser(id: number){
   return this._http.get(`${environment.apiUrl}/users/${id}`)
  }
}
