import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserListService {

  constructor(
    private _http: HttpClient
  ) { }

  public getUserList(page: number){
    return this._http.get(`${environment.apiUrl}/users?page=${page}`);
  }
}
