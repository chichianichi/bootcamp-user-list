import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../services/login/login.service';
/*Importamos la variable que contiene la url a consultar */
import { environment } from '../../../environments/environment'
import { Login } from '../../models/Login';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  h3Show = false;
  loginForm = this._formBuilder.group(
    {
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    }
  )


  constructor(
    /*Todos los servicios se deben inicializar en el constructor*/
    private _formBuilder: FormBuilder,
    private _loginService: LoginService,
    private _cookieService: CookieService    
  ) { 
    
  }

  ngOnInit(): void {
    console.log(environment.apiUrl)
  }

  onSubmit(): void {
    const isFormValid = this.loginForm.valid
    if (isFormValid) {
      console.log('ITS VALID!');
      this.h3Show = false;
      const body: Login = {
        email: this.loginForm.controls['email'].value,
        password: this.loginForm.controls['password'].value,
      }
      this._loginService.login(body).subscribe((response) => {
        console.log(response)
        /*Guardamos el token en el cookies */
        this._cookieService.set('token', response.token);
        window.location.href = `/user-list`
      })
    } else {
      this.h3Show = true;
    }
  }

}
