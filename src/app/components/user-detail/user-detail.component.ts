import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { CookieService } from 'ngx-cookie-service';
import { UserDetailService } from '../../services/user-detail/user-detail.service';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  userData: User[] = [];
  hideUserData: boolean = true;
  hideAlertMessage: boolean = true;

  constructor(
    private _userDetailService: UserDetailService,
    private _cookieService: CookieService,
    private route: ActivatedRoute
  ) { 
    /*Aqui indicamos que recibiremos parametros y ejecutamos
    la funcion que va a hacer la querry a la api */
    this.route.params
      .subscribe( params => this.getUser(params['id']));
  }

  ngOnInit(): void {
    if(!this._cookieService.get('token')){
      return;
    }
    this.hideUserData = false;         
  }

  getUser(id: any){
    this._userDetailService.getUser(id)
      .subscribe((response: any) => {
        this.userData[0]  = response;
      });   
  }

}
