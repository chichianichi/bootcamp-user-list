import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { UserListService } from '../../services/user-list/user-list.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {  
  hideUserList: boolean = true;
  hideAlertMessage: boolean = true;
  users: any[]= [];
  page: number = 1;
  
  
  constructor(
    private _cookieService: CookieService,
    private _userListService: UserListService,
    private router: Router
  ) {  }

  ngOnInit(): void {
    if(!this._cookieService.get('token')){
      this.hideAlertMessage = false;
      return;
    }
    this.hideUserList = false;
    
    this.getList();      
  }

  viewUserDetails(id: any){
    //la siguiente linea nos redirige a la ruta y
    //le manda el parametro id
    this.router.navigate(['user-detail',id]);
  }

  getList(){
    
    this._userListService.getUserList(this.page)
    .subscribe((response) => {
     this.users = Object.values(response)[4];
    }); 
  }

  nextPage(){
    this.page ++;
    this.getList();
  }

  prevPage(){
    this.page --;
    this.getList();
  }

}
